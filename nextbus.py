#!/usr/bin/env python
"""Return the time in minutes to the next bus
"""

import json
import urllib2
import sys
import time
import re

# Initial setup:
# --------------

# Constants
API_URL = "http://svc.metrotransit.org/NexTrip"
DICT_OF_DIRECTIONS = {"south":1, "east":2, "west":3, "north":4}
DEBUG = False

# Functions
# ---------

def usage(error):
    """Print usage and exit. Used for help and errors."""
    print "Usage: %s <routeName> <stopName> <directionName>" % sys.argv[0]
    sys.exit(error)

def get_api_response(query_path):
    """Function to query API"""
    request = urllib2.Request("%s/%s" % (API_URL, query_path))
    request.add_header("Content-Type", "application/json")
    request.add_header("Accept", "application/json")

    return json.load(urllib2.urlopen(request))

def get_route_id_from_name(route_name):
    """Get route number from plain-text name
    Queries baseURL/routes
    Searches for Description = routeName, Route = routeID

    Arguments:
    route_name -- the human-readable name of the route
    """
    query_path = "Routes"
    list_of_routes = get_api_response(query_path)
    for route in list_of_routes:
        if route["Description"] == route_name:
            return route["Route"]
    return

def get_stop_id_from_name(route_id, stop_name, direction_id):
    """Get stop ID from plain-text name
    Queries baseURL/stops/{routeID}
    Searches for "Text":"{stopName}, "Value":"{stopID}"

    Arguments:
    route_id -- The uniqe alphanumeric ID for the route
    stop_name -- The human-readable name of the bus stop
    direction_id -- The unique intiger ID for the direction of the route
    """
    query_path = "stops/%s/%s" % (route_id, direction_id)
    list_of_stops = get_api_response(query_path)
    for stop in list_of_stops:
        if stop["Text"] == stop_name:
            return stop["Value"]
    return

def get_next_bus(route_id, stop_id, direction_id):
    """Function to get next routeID bus stopping at stopID going stopDirection
    Queries baseURL/routeID/direction/stopID
    Searches for [0] DepartureText:"time"

    Arguments:
    route_id -- The uniqe alphanumeric ID for the route
    stop_id -- The unique intiger ID for the bus stop
    direction_id -- The unique intiger ID for the direction of the route
    """
    query_path = "%s/%s/%s" % (route_id, direction_id, stop_id)
    list_of_departures = get_api_response(query_path)
    next_bus = re.findall(r"\d{13}", list_of_departures[0]["DepartureTime"])
    if len(next_bus) > 0:
        return int(next_bus[0]) / 1000
    else:
        return

def get_minutes_to_time(time_target):
    """Returns the time in imutes from now until the epoch time provided"""
    time_now = int(time.time())
    time_difference = time_target - time_now
    minutes = time_difference / 60
    return minutes

def main():
    """ Main method """
    # Check to see if the user is asking for help or needs help.
    if len(sys.argv) > 1:
        if sys.argv[1] == "--help" or sys.argv[1] == "-h":
            usage(0)
    elif len(sys.argv) != 4:
        usage(1)

    # Set variables from input arguments.
    (route_name, stop_name, direction_name) = sys.argv[1:4]

    if direction_name.lower() in DICT_OF_DIRECTIONS:
        direction_id = DICT_OF_DIRECTIONS[direction_name.lower()]
    else:
        directions = " ".join(["%s" % key for key in DICT_OF_DIRECTIONS.keys()])
        print "%s is not a valid route direction." % direction_name
        print "Valid direction names are: %s." % directions
        usage(2)

    # Actual work
    # -----------
    route_id = get_route_id_from_name(route_name)
    if route_id is None:
        if DEBUG:
            print "The route '%s' is not running today or does not exist." % route_name
        sys.exit(3)

    stop_id = get_stop_id_from_name(route_id, stop_name, direction_id)
    if stop_id is None:
        if DEBUG:
            print "The stop '%s' is not on the '%s' route." % (stop_name, route_name)
        sys.exit(4)

    next_bus = get_next_bus(route_id, stop_id, direction_id)
    if next_bus is None:
        if DEBUG:
            print "There does not appear to be a '%s' bus heading '%s' stopping at '%s' today." % (
                route_name, direction_name, stop_name)
        sys.exit(5)

    print "%s minutes" % get_minutes_to_time(next_bus)

if __name__ == "__main__":
    main()
